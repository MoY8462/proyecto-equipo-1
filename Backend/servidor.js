var express = require('express');
var app = express();
var cors = require('cors');
var admin = require('firebase-admin');

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: 'https://iot-pet-76a7c.firebaseio.com/'
});

var db=admin.database();

app.options('*', cors())

var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}



app.get('/dht11',cors(corsOptions), (req, res) =>{
 
   var data=JSON.parse(req.query.data);
   console.log(data);
   db.ref("users").orderByChild("idParticle").equalTo(req.query.coreid).on("child_added", (snap) => {
    var uid= snap.val().uid;
    db.ref("users/"+uid+"/datos/HumTemp").push(data);
    });
   res.send(JSON.stringify("{'mensaje':Recibido}"));
})



app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});



app.get('/mq2',cors(corsOptions), (req,res)=>{
    
    var gas=JSON.parse(req.query.data);
    console.log(gas)
    db.ref("users").orderByChild("idParticle").equalTo(req.query.coreid).on("child_added", (snap) => {
    var uid= snap.val().uid;
    db.ref("users/"+uid+"/datos/Gas").push(gas);
    });
    res.send(JSON.stringify("{'mensaje':gas recibido}"));
})




app.get('/tanqueCroquetas',cors(corsOptions), (req,res)=>{
    var h=JSON.parse(req.query.data);
    console.log(h)
    db.ref("users").orderByChild("idParticle").equalTo(req.query.coreid).on("child_added", (snap) => {
    var uid= snap.val().uid;
    db.ref("users/"+uid+"/datos/altCroq").push(h);
    });
    res.send(JSON.stringify("{'mensaje':altura croquetas recibido}"));
})


app.get('/tanqueAgua',cors(corsOptions), (req,res)=>{
    var ta=JSON.parse(req.query.data);
    console.log(ta)
    db.ref("users").orderByChild("idParticle").equalTo(req.query.coreid).on("child_added", (snap) => {
    var uid= snap.val().uid;
    db.ref("users/"+uid+"/datos/altAgua").push(ta);
    });
    res.send(JSON.stringify("{'mensaje':altura agua recibido}"));
})

app.get('/ProxAlim',cors(corsOptions), (req,res)=>{
    var pa=JSON.parse(req.query.data);
    console.log(pa)
    db.ref("users").orderByChild("idParticle").equalTo(req.query.coreid).on("child_added", (snap) => {
    var uid= snap.val().uid;
    db.ref("users/"+uid+"/datos/proxAlim").push(pa);
    });
    res.send(JSON.stringify("{'mensaje':mascota comiendo recibido}"));
})

app.get('/ProxAgua',cors(corsOptions), (req,res)=>{
    var pagua=JSON.parse(req.query.data);
    console.log(pagua)
    db.ref("users").orderByChild("idParticle").equalTo(req.query.coreid).on("child_added", (snap) => {
    var uid= snap.val().uid;
    db.ref("users/"+uid+"/datos/proxAgua").push(pagua);
    });
    res.send(JSON.stringify("{'mensaje': mascota bebiendo recibido}"));
})