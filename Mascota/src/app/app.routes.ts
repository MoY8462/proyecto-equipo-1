import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from "./user/user.component";
import { LoginComponent } from "./login/login.component";
import { AuthenticationGuard } from './services/authentication.guard';
import { HomeComponent } from "./home/home.component";
import { ContactoComponent } from "./contacto/contacto.component";
import { RegistroComponent } from "./registro/registro.component";
import { SobreNosotrosComponent } from "./sobre-nosotros/sobre-nosotros.component";
import { TemperatursaComponent } from "./temperatursa/temperatursa.component";
import { PromocionesComponent } from './promociones/promociones.component';


const app_routes: Routes = [
    { path: 'user', component: UserComponent, canActivate: [AuthenticationGuard] },
    {path: '', component: HomeComponent},
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent },
    { path: 'temperatura', component: TemperatursaComponent },
    { path: 'contacto', component: ContactoComponent },
    { path: 'registro', component: RegistroComponent },
    { path: 'sobrenosotros', component: SobreNosotrosComponent },
    { path: 'promociones', component: PromocionesComponent},
    { path: '**', pathMatch: 'full', redirectTo: ''},
    ];
    
export const app_routing = RouterModule.forRoot(app_routes);