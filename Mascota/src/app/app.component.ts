import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Mascota';
  constructor(private router:Router){}
  model = {
    left: true,
    middle: false,
    right: false
  };

}
