import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string = "";
  password: string = "";
  registrarse: boolean = false;

  constructor(private authenticationService: AuthenticationService,private router: Router) { }

  ngOnInit(): void {
  }
  Registro(){
    this.router.navigate(['registro']);
  }
  Login(){
    this.authenticationService.login(this.email, this.password).then
    (
      (data)=>
      {
        alert('Loggeado correctamente');
        console.log(data);
        this.router.navigate(['user']);
      }
    ).catch((error)=>
    {
      alert('Ocurrió un error');
      console.log(error);
    });
  }
}
