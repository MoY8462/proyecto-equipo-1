import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemperatursaComponent } from './temperatursa.component';

describe('TemperatursaComponent', () => {
  let component: TemperatursaComponent;
  let fixture: ComponentFixture<TemperatursaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemperatursaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemperatursaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
