import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

 
  constructor(private angularDatabase: AngularFireDatabase) { }

  getHumTemp(uid)
  {
    return this.angularDatabase.list("/users/"+uid+"/datos/HumTemp");
  }

  getGas(uid)
  {
    return this.angularDatabase.list("/users/"+uid+"/datos/Gas");
  }

  getAltCroq(uid)
  {
    return this.angularDatabase.list("/users/"+uid+"/datos/altCroq");
  }
  
  getAltAgua(uid)
  {
    return this.angularDatabase.list("/users/"+uid+"/datos/altAgua");
  }
  
  getProxAgua(uid)
  {
    return this.angularDatabase.list("/users/"+uid+"/datos/proxAgua");
  }
  
  getProxAlim(uid)
  {
    return this.angularDatabase.list("/users/"+uid+"/datos/proxAlim");
  }
  getName(uid)
  {
     return this.angularDatabase.list("/users/"+uid);
  }
}
