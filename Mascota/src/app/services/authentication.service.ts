import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  public dato1;
  public dato2;  
  public dato3;
  public dato4;
  public dato5;
  public dato6;

  
  constructor(private angularFireAuth: AngularFireAuth, private angularFireDatabase: AngularFireDatabase) { }

  createUser(user){
    return this.angularFireDatabase.object('/users/'+ user.uid).set(user);
  }
  
  login(email:string, password: string)
  {
    return this.angularFireAuth.auth.signInWithEmailAndPassword(email, password);
  }

  register(email:string, password: string)
  {
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  getStatus()
  {
    return this.angularFireAuth.authState;
  }

  logOut()
  {
    return this.angularFireAuth.auth.signOut();
  }
  
}
