import { Component, OnInit, AfterViewInit  } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { DatabaseService } from '../services/database.service';
import { AuthenticationGuard } from '../services/authentication.guard';
import { element } from 'protractor';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})


export class UserComponent implements OnInit {


  HumTemp:{};
  Gas:{};
  altCroq:{};
  altAgua:{};
  proxAlim:{};
  proxAgua:{};
  //Estos contienen todos los datos
  fruitsG: string[]; //Temperatura
  fruitsH: string [];
  fruitsF: string [];
  fruitsHor: string [];
  alarmaG: string []; //Sensor de gas
  alarmaF: string [];
  alarmaH: string [];
  altBotG: string[]; //Es porcentaje en el plato de comida
  horaBotG: string[];
  fechaBotG: string[];
  altAG: string[];  //Es porcentaje en el plato de agua
  horaAG: string[];
  fechaAG: string[];
  distanciaAguaG: string[];
  horaAguaG: string[];
  fechaAguaG: string[];
  distanciaAlimentoG: string[];
  horaAlimentoG: string[];
  fechaAlimentoG: string[];
  Name:{};
  nombre:string;
  mascota:string;
  
  public isCollapsed = true;
  public isCollapsed2 = true;
  public isCollapsed3 = true;
  public isCollapsed4 = true;
  public isCollapsed5 = true;
  public isCollapsed6 = true;
  
  constructor(private authenticationService:AuthenticationService, private router:Router,
     private databaseService: DatabaseService, private guard:AuthenticationGuard) { 
       this.obtenerDatos();

     }

  ngOnInit(){

  }
  
  //Extrae los datos del usuario de la base
  obtenerDatos()
  {
    var uid=this.guard.id;
    console.log(uid);
    
    this.databaseService.getHumTemp(uid).valueChanges().subscribe((data)=>
        {
          this.HumTemp = data;
          let fruits: string[] = [];
          let fruitsh: string[] = [];
          let fruitsf: string[] = [];
          let fruitshor: string[] = [];
          for (var _i = Object.keys(this.HumTemp).length - 10; _i <= Object.keys(this.HumTemp).length - 1; _i++) {
            fruits.push(this.HumTemp[_i].Temperatura);
            fruitsh.push(this.HumTemp[_i].Humedad);
            fruitsf.push(this.HumTemp[_i].Fecha);
            fruitshor.push(this.HumTemp[_i].Hora);
          }
          
          this.fruitsG = fruits;
          this.fruitsF = fruitsf;
          this.fruitsH = fruitsh;
          this.fruitsHor = fruitshor;
        }, (error)=>
        {
          console.log(error);
        });
        
    this.databaseService.getGas(uid).valueChanges().subscribe((data)=>
        {
          this.Gas=data;
          let alarma: string[] = [];
          let fecha: string[] = [];
          let hora: string[] = [];
          for (var _i = Object.keys(this.Gas).length - 3; _i <= Object.keys(this.Gas).length - 1; _i++) {
            alarma.push(this.Gas[_i].Mensaje);
            fecha.push(this.Gas[_i].Fecha);
            hora.push(this.Gas[_i].Hora);
          }
          this.alarmaG = alarma;
          this.alarmaF = fecha;
          this.alarmaH = hora;
          console.log(this.alarmaG);
        }, (error)=>
        {
          console.log(error);
        });
        
    this.databaseService.getAltCroq(uid).valueChanges().subscribe((data)=>
        {
          this.altCroq=data;
          let altBot: string[] = [];
          let horaBot: string[] = [];
          let fechaBot: string[] = [];
          for (var _i = Object.keys(this.altCroq).length - 10; _i <= Object.keys(this.altCroq).length - 1; _i++) {
            altBot.push(this.altCroq[_i].CantidadDeAlimento);
            horaBot.push(this.altCroq[_i].Hora);
            fechaBot.push(this.altCroq[_i].Fecha);
          }
          this.altBotG = altBot;
          this.horaBotG = horaBot;
          this.fechaBotG = fechaBot;
          //console.log(this.altBotG[0]);
        }, (error)=>
        {
          console.log(error);
        });
        
        
        
    this.databaseService.getAltAgua(uid).valueChanges().subscribe((data)=>
        {
          this.altAgua=data;
          let altA: string[] = [];
          let horaA: string[] = [];
          let fechaA: string[] = [];
          for (var _i = Object.keys(this.altAgua).length - 10; _i <= Object.keys(this.altAgua).length - 1; _i++) {
            altA.push(this.altAgua[_i].CantidadDeAgua);
            horaA.push(this.altAgua[_i].Hora);
            fechaA.push(this.altAgua[_i].Fecha);
          }
          this.altAG = altA;
          this.horaAG = horaA;
          this.fechaAG = fechaA;
        }, (error)=>
        {
          console.log(error);
        });
        
        
    this.databaseService.getProxAgua(uid).valueChanges().subscribe((data)=>
        {
          this.proxAgua=data;
          let distanciaAgua: string[] = [];
          let horaAgua: string[] = [];
          let fechaAgua: string[] = [];
          for (var _i = Object.keys(this.proxAgua).length - 6; _i <= Object.keys(this.proxAgua).length - 1; _i++) {
            distanciaAgua.push(this.proxAgua[_i].Mensaje);
            horaAgua.push(this.proxAgua[_i].Hora);
            fechaAgua.push(this.proxAgua[_i].Fecha);
          }
          this.distanciaAguaG = distanciaAgua;
          this.horaAguaG = horaAgua;
          this.fechaAguaG = fechaAgua;
        }, (error)=>
        {
          console.log(error);
        });
        
        
        
    this.databaseService.getProxAlim(uid).valueChanges().subscribe((data)=>
        {
          this.proxAlim=data;
          let distanciaAlimento: string[] = [];
          let horaAlimento: string[] = [];
          let fechaAlimento: string[] = [];
          for (var _i = Object.keys(this.proxAlim).length - 6; _i <= Object.keys(this.proxAlim).length - 1; _i++) {
            distanciaAlimento.push(this.proxAlim[_i].Mensaje);
            horaAlimento.push(this.proxAlim[_i].Hora);
            fechaAlimento.push(this.proxAlim[_i].Fecha);
          }
          this.distanciaAlimentoG = distanciaAlimento;
          this.horaAlimentoG = horaAlimento;
          this.fechaAlimentoG = fechaAlimento;
        }, (error)=>
        {
          console.log(error);
        });
        
    this.databaseService.getName(uid).valueChanges().subscribe((data)=>
        {
            this.Name=data;
            this.nombre=this.Name[4];
            this.mascota=this.Name[3];
            console.log(this.nombre);
            console.log(this.mascota);
        });



  }
  
  logout()
  {
    this.authenticationService.logOut().then(()=>
    {
      alert('Sesión cerrada');
      this.router.navigate(['login']);
    }).catch((error)=>{console.log(error)})
  }
  
  
  
  
}

